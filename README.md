# Planning Items

In Paraguay, the intended items to be procured are disclosed already at the 
planning stage, in a high level. Those items are not the same as the ones 
published in the tender stage because the planning items are in a higher 
classification level than the tender ones.

## Example

```json
{
    "planning": {
        "items": {
                "items": [
                    {
                        "id": "60ac4b7529dfb95b73a8f37310a21e1f",
                        "description": "Impresion de papeleria, formularios, tarjetas",
                        "classification": {
                            "scheme": "x_catalogoNivel4DNCP",
                            "id": "82121507",
                            "description": "Impresion de papeleria, formularios, tarjetas ",
                            "uri": "https://www.contrataciones.gov.py/datos/api/v2/doc/catalogo/nivel-4/82121507"
                        }
                    }
                ]
        }
    },
    "tender": {
         "items": [
            {
                "id": "82121507-010-1-1",
                "description": "Informe de Gestión   Anual en formato impreso y digital",
                "classification": {
                    "scheme": "x_catalogoNivel5DNCP",
                    "id": "82121507-010",
                    "description": "Impresion de libros o registros",
                    "uri": "https://www.contrataciones.gov.py/datos/api/v2/doc/catalogo/nivel-5/82121507-010"
                }
            }
        ]
    }
    
}
```